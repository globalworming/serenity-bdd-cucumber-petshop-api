Feature: Pet Profile

  Scenario:  As a Store Owner, I would like add an image to pet profiles because data shows more sales with cute photos
    Given store owner with inventory

      | categoryId | categoryName | petName | tagsId | tagsName | status    |
      | 0          | dog          | doggie  | 0      | test     | available |

    When store owner uploads a pet image for latest created pet
    Then store owner should see that latest created pet has an image

