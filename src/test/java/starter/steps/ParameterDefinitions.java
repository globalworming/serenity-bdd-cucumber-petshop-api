package starter.steps;

import client.api.PetApi;
import io.cucumber.java.Before;
import io.cucumber.java.ParameterType;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.actors.OnStage;
import net.serenitybdd.screenplay.actors.OnlineCast;

public class ParameterDefinitions {

    @ParameterType(".*")
    public Actor actor(String actorName) {
        return OnStage.theActorCalled(actorName);
    }

    @Before
    public void setTheStage() {
        OnlineCast cast = new OnlineCast();
        // quick workaround, would solve that with abilities usually
        cast.actorNamed("store owner").remember("petApiClient", new PetApi());
        OnStage.setTheStage(cast);
    }
}
