package starter.steps;

import client.ApiException;
import client.ApiResponse;
import client.api.PetApi;
import client.model.Category;
import client.model.ModelApiResponse;
import client.model.Pet;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.serenitybdd.screenplay.Actor;
import org.hamcrest.collection.IsIterableWithSize;
import starter.performables.CreateInventory;

import java.io.File;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;

public class PetShopStepDefinitions {


    // overkill, creating a single pet would be sufficient for the ione scenario
    @Given("{actor} with inventory")
    public void actorWithInventory(Actor actor, List<Map<String, String>> dataTable) {
        // TODO build table transformer
        List<Pet> inventoryItems = dataTable.stream().map(datatableRowToPetMapper).collect(Collectors.toList());
        actor.attemptsTo(new CreateInventory(inventoryItems));
    }

    @When("{actor} uploads a pet image for latest created pet")
    public void actorUploadsPetImage(Actor actor) throws ApiException {

        String resourceName = "pet.jpg";

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(resourceName).getFile());
        PetApi petApiClient = actor.recall("petApiClient");
        ApiResponse<ModelApiResponse> latestAddedPetId = petApiClient.uploadFileWithHttpInfo(actor.recall("latestAddedPetId"), "", file);
    }

    @Then("{actor} should see that latest created pet has an image")
    public void thePetProfileHasImage(Actor actor) {
        actor.should(seeThat("pet images", (a) -> {
            PetApi petApiClient = actor.recall("petApiClient");
            try {
                long latestAddedPetId = actor.recall("latestAddedPetId");
                return petApiClient.getPetById(latestAddedPetId).getPhotoUrls();
            } catch (ApiException e) {
                throw new RuntimeException(e);
            }
        }, IsIterableWithSize.iterableWithSize(1)));
    }

    Function<Map<String, String>, Pet> datatableRowToPetMapper = row -> {
        Pet pet = new Pet();
        Category category = new Category();
        category.id(Long.valueOf(row.get("categoryId")));
        pet.category(category);
        pet.name(row.get("petName"));
        // TODO map the rest
        return pet;
    };
}
