package starter.performables;

import client.ApiException;
import client.ApiResponse;
import client.api.PetApi;
import client.model.Pet;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import net.serenitybdd.core.Serenity;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;

import java.util.List;

public class CreateInventory implements Performable {
    private final List<Pet> inventoryItems;

    public CreateInventory(List<Pet> inventoryItems) {
        this.inventoryItems = inventoryItems;
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        PetApi petApiClient = actor.recall("petApiClient");
        Serenity.reportThat("create "  + inventoryItems.size() + " pets", () -> inventoryItems.forEach(pet -> {
            try {
                ApiResponse<String> response = petApiClient.addPetWithHttpInfo(pet);
                actor.remember("latestAddedPetId", new Gson().fromJson(response.getData(), JsonObject.class).get("id").getAsLong());
            } catch (ApiException e) {
                throw new RuntimeException(e);
            }}));
    }
}
